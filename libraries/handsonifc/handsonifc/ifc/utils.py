import uuid

from ifcopenshell.guid import compress

def create_guid():
    """Return a new guid"""
    return compress(uuid.uuid1().hex)


O = 0.0, 0.0, 0.0
X = 1.0, 0.0, 0.0
Y = 0.0, 1.0, 0.0
Z = 0.0, 0.0, 1.0


def create_ifcaxis2placement(ifcfile, point=O, dir1=Z, dir2=X):
    """Creates an IfcAxis2Placement3D from Location, Axis and RefDirection specified as Python tuples"""
    return ifcfile.createIfcAxis2Placement3D(
        ifcfile.createIfcCartesianPoint(point),
        ifcfile.createIfcDirection(dir1),
        ifcfile.createIfcDirection(dir2),
    )


def create_ifclocalplacement(ifcfile, point=O, dir1=Z, dir2=X, relative_to=None):
    """Creates an IfcLocalPlacement from Location, Axis and RefDirection, specified as Python tuples, and relative placement"""
    axis2placement = create_ifcaxis2placement(ifcfile, point, dir1, dir2)
    return ifcfile.createIfcLocalPlacement(relative_to, axis2placement)


def create_ifcpolyline(ifcfile, point_list):
    """Creates an IfcPolyLine from a list of points, specified as Python tuples"""
    ifcpts = [ifcfile.createIfcCartesianPoint(point) for point in point_list]
    return ifcfile.createIfcPolyLine(ifcpts)


def create_ifcextrudedareasolid(
    ifcfile, point_list, ifcaxis2placement, extrude_dir, extrusion
):
    """Creates an IfcExtrudedAreaSolid from a list of points, specified as Python tuples"""
    polyline = create_ifcpolyline(ifcfile, point_list)
    ifcclosedprofile = ifcfile.createIfcArbitraryClosedProfileDef(
        "AREA", None, polyline
    )
    ifcdir = ifcfile.createIfcDirection(extrude_dir)
    return ifcfile.createIfcExtrudedAreaSolid(
        ifcclosedprofile, ifcaxis2placement, ifcdir, extrusion
    )
