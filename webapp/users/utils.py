import os, re
import json
import secrets
import zipfile
import subprocess
from time import sleep
from PIL import Image
from flask import url_for, current_app, flash
from flask_login import current_user
from flask_mail import Message
from webapp import mail
from handsonifc.cli.createIfc import createIfcCmd


def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(current_app.root_path, 'static/avatars', picture_fn)

    output_size = (125, 125)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn

def send_reset_email(user):
    token = user.get_reset_token()
    with open('/workspace/secrets/config.json') as config_file:
        config = json.load(config_file)
    sender = config.get("MAIL_USERNAME")
    msg = Message('Password Reset Request',
                  sender=sender, recipients=[user.email])
    msg.body = f'''To reset your password, visit the following link:
{url_for('users.reset_token', token=token, _external=True)}

If you did not make this request then simply ignore this email and no changes will be made.
'''
    mail.send(msg)

def save_archive(form_archive):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_archive.filename)
    archive_fn = random_hex + f_ext
    archive_path = os.path.join(current_app.root_path, 'static/data', archive_fn)
    form_archive.save(archive_path)
    return archive_path

def extract_archive(archive_path):
    folder_path = os.path.join(current_app.root_path, 'static/data', current_user.username)
    with zipfile.ZipFile(archive_path, 'r') as zipped_archive:
        filenames = zipped_archive.namelist()
        regex = '^.*\.(dxf|ts|obj|stl|json|xlsx).*$'
        considered_files = [filename for filename in filenames if re.search(regex, filename)]
        for considered_file in considered_files:
            zipped_archive.extract(considered_file, folder_path)
    os.remove(archive_path)

def process_ifc():
    filename = ''
    creator = 'Michael Koebberich'
    organization = 'GeoQuo GmbH'
    project_name = 'HandsOnIfc'
    sidecar = '.xlsx'
    shift_x = 0
    shift_y = 0
    shift_z = 0

    path = os.path.join(current_app.root_path, 'static/data', current_user.username)
    destination = os.path.join(path, 'result.ifc')
    report = createIfcCmd(path, destination, filename, creator, organization, project_name, sidecar, shift_x, shift_y, shift_z)
    return report
    