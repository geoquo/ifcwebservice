from flask import render_template, request, Blueprint
from webapp.models import Post

main = Blueprint('main', __name__)


@main.route("/")
@main.route("/about")
def about():
    return render_template('about.html', title='About')

@main.route("/news")
def news():
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)
    return render_template('news.html', posts=posts)