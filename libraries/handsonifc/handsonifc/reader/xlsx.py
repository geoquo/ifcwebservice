import json
from openpyxl import load_workbook
from .base import SidecarReader


class XLSXReader(SidecarReader):

    def _read(self, path):
        result = {}
        book = load_workbook(path)

        # Read config sheet    
        sheet = book['config']
        rows = sheet.rows
        
        headers = [cell.value for cell in next(rows)]
        columns = [[] for _ in range(len(headers))]

        for row in rows:
            values = [cell.value for cell in row]
            for index, value in enumerate(values):
                columns[index].append(value)

        for index, header in enumerate(headers):
            result[header] = columns[index]

        # Read property sets
        sheets_list = book.sheetnames
        sheets_list.remove('config')

        psets = []
        for sheet_name in sheets_list:
            sheet = book[sheet_name]
            rows = sheet.rows
        
            attributes = []
            headers = [cell.value for cell in next(rows)]
            for row in rows:
                attribute = {}
                values = [cell.value for cell in row]
                for index, header in enumerate(headers):
                    attribute[header] = values[index]
                attributes.append(attribute)

            pset = {}
            for index, sheet in enumerate(sheets_list):
                pset['name'] = sheet_name
                pset['attributes'] = attributes
            psets.append(pset)
        
        # Combine config and psets
        result['psets'] = psets
        return result