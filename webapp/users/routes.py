import os
import time
from time import sleep
import shutil
from flask import render_template, url_for, current_app, flash, redirect, request, Blueprint, send_file
from flask_login import login_user, current_user, logout_user, login_required
from webapp import db, bcrypt
from webapp.models import User, Post
from webapp.users.forms import (RegistrationForm, LoginForm, UpdateAccountForm,
                                   RequestResetForm, ResetPasswordForm, ConvertData)
from webapp.users.utils import (save_picture, send_reset_email, save_archive, extract_archive,
                                process_ifc)                   

users = Blueprint('users', __name__)

@users.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('main.news'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in', 'success')
        return redirect(url_for('users.login'))
    return render_template('register.html', title='Register', form=form)


@users.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('main.news'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('main.news'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Login', form=form)


@users.route("/logout")
def logout():
    if current_user.username in os.listdir(os.path.join(current_app.root_path, 'static/data')):
        shutil.rmtree(os.path.join(current_app.root_path, 'static/data', current_user.username))
    logout_user()
    return redirect(url_for('main.news'))


@users.route("/account", methods=['GET', 'POST'])
@login_required
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        db.session.commit()
        flash('Your account has been updated!', 'success')
        return redirect(url_for('users.account'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    image_file = url_for('static', filename='avatars/' + current_user.image_file)
    return render_template('account.html', title='Account',
                           image_file=image_file, form=form)


@users.route("/user/<string:username>")
def user_posts(username):
    page = request.args.get('page', 1, type=int)
    user = User.query.filter_by(username=username).first_or_404()
    posts = Post.query.filter_by(author=user)\
        .order_by(Post.date_posted.desc())\
        .paginate(page=page, per_page=5)
    return render_template('user_posts.html', posts=posts, user=user)


@users.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:
        return redirect(url_for('main.news'))
    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        flash('An email has been sent with instructions to reset your password.', 'info')
        return redirect(url_for('users.login'))
    return render_template('reset_request.html', title='Reset Password', form=form)


@users.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if current_user.is_authenticated:
        return redirect(url_for('main.news'))
    user = User.verify_reset_token(token)
    if user is None:
        flash('That is an invalid or expired token', 'warning')
        return redirect(url_for('users.reset_request'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user.password = hashed_password
        db.session.commit()
        flash('Your password has been updated! You are now able to log in', 'success')
        return redirect(url_for('users.login'))
    return render_template('reset_token.html', title='Reset Password', form=form)

@users.route("/convert", methods=['GET', 'POST'])
@login_required
def convert():
    form = ConvertData()
    if form.validate_on_submit():
        if form.archive.data:
            flash('Your data has been uploaded and is being processed!', 'success')
            if current_user.username in os.listdir(os.path.join(current_app.root_path, 'static/data')):
                shutil.rmtree(os.path.join(current_app.root_path, 'static/data', current_user.username))
            archive_path = save_archive(form.archive.data)
            extract_archive(archive_path)
            try:
                report = process_ifc()
                for entry in report:
                    flash(entry, 'success')
            except Exception:
                print('Invalid data! Check the collection of file you provided.')
                flash('Invalid data! Check the collection of file you provided.', 'danger')
                shutil.rmtree(os.path.join(current_app.root_path, 'static/data', current_user.username))
        else:
            flash('No data has been provided for conversion! Please browse your file system to select a zip archive.', 'warning')
        return redirect(url_for('users.convert'))
    
    
    user_data_directories = os.listdir(os.path.join(current_app.root_path, 'static/data'))
    user_data = True if current_user.username in user_data_directories else False
    return render_template('convert.html', title='Convert', form=form, user_data=user_data)

@users.route("/download", methods=['GET'])
@login_required
def download():

    if current_user.username in os.listdir(os.path.join(current_app.root_path, 'static/data')):
        path = os.path.join(current_app.root_path, 'static/data', current_user.username, 'result.ifc')
        try: 
            return send_file(path, as_attachment=True)
        finally:
            shutil.rmtree(os.path.join(current_app.root_path, 'static/data', current_user.username))
    else:
        flash('Your data is only stored until you download it. Provide a new dataset to continue.', 'danger')
        return redirect(url_for('users.convert'))