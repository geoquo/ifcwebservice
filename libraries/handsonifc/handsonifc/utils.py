import functools
import time

def measure_time(function):

    @functools.wraps(function)
    def wrapped(*args, **kwargs):
        start_time = time.time()
        res = function(*args, **kwargs)
        return res, (time.time() - start_time)
    
    return wrapped
