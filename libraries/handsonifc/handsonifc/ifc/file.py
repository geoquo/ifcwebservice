from contextlib import contextmanager
from pathlib import Path
import tempfile

import ifcopenshell

from .template import get_template
from .utils import create_guid, create_ifclocalplacement


@contextmanager
def get_tempfile(suffix=None, dir=None):
    temp_file = Path(tempfile.mkstemp(suffix=suffix, dir=dir)[1])
    yield temp_file
    temp_file.unlink()


@contextmanager
def new_ifcfile(
    inputs,
    output,
    filename,
    creator,
    organization,
    project_name,
    shift_x,
    shift_y,
    shift_z,
    timestamp=None,
    elevation=0.0,
):
    template = get_template(filename, creator, organization, project_name, timestamp)
    with get_tempfile(suffix=".ifc") as temp_file:
        temp_file.write_text(template)

        # Obtain references to instances defined in template
        ifcfile = ifcopenshell.open(str(temp_file))
        owner_history = ifcfile.by_type("IfcOwnerHistory")[0]
        project = ifcfile.by_type("IfcProject")[0]
        # IFC hierarchy creation
        site_placement = create_ifclocalplacement(ifcfile)
        site = ifcfile.createIfcSite(create_guid(), owner_history, "Site", None, None, site_placement, None, None, "ELEMENT", None, None, None, None, None)
        building_placement = create_ifclocalplacement(ifcfile, relative_to=site_placement)
        building = ifcfile.createIfcBuilding(create_guid(), owner_history, "Building", None, None, building_placement, None, None, "ELEMENT", None, None, None)
        ifcfile.createIfcRelAggregates(create_guid(), owner_history, "Project Container", None, project, [site])
        ifcfile.createIfcRelAggregates(create_guid(), owner_history, "Site Container", None, site, [building])

        yield ifcfile
        ifcfile.write(output)
